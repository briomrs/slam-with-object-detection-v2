#ifndef PEOPLEDETECT_H
#define PEOPLEDETECT_H

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

namespace ORB_SLAM2
{
	class PeopleDetection
	{
	private:
		std::vector<std::string> classes;
		int mWidth;
		int mHeight;
		cv::dnn::Net mNet;
		std::vector<cv::String> GetOutputsNames(const cv::dnn::Net& net);
		std::vector<cv::String> mClassNames;
		std::vector<cv::Rect> postprocess(const cv::Mat& frame, const std::vector<cv::Mat>& outs);
		const char * mClassesFile;
		const char * mModelConfiguration;
		const char * mModelWeights;

		const double mScaleFactor = 1.0 / 255.0;
		const float mConfThreshold = 0.2f; // Confidence threshold
	public:
		PeopleDetection(const char * classesFilePath, const char * modelConfigurationFilePath, const char * modelWeightsFilePath);
		~PeopleDetection();

		std::vector<cv::Rect> Predict(const cv::Mat & frame);	
	};
}
#endif // PEOPLEDETECTION_H
	

