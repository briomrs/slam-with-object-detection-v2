#include "PeopleDetect.h"

namespace ORB_SLAM2
{
	std::vector<cv::String> PeopleDetection::GetOutputsNames(const cv::dnn::Net & net)
	{
		static std::vector<cv::String> names;
		if (names.empty())
		{
			//Get the indices of the output layers, i.e. the layers with unconnected outputs
			std::vector<int> outLayers = net.getUnconnectedOutLayers();

			//get the names of all the layers in the network
			std::vector<cv::String> layersNames = net.getLayerNames();

			// Get the names of the output layers in names
			names.resize(outLayers.size());
			for (size_t i = 0; i < outLayers.size(); ++i)
				names[i] = layersNames[outLayers[i] - 1];
		}
		return names;
	}

	std::vector<cv::Rect> PeopleDetection::postprocess(const cv::Mat & frame, const std::vector<cv::Mat>& outs)
	{
		std::vector<cv::Rect> boxes;

		for (size_t i = 0; i < outs.size(); ++i)
		{
			// Scan through all the bounding boxes output from the network and keep only the
			// ones with high confidence scores. Assign the box's class label as the class
			// with the highest score for the box.
			float* data = (float*)outs[i].data;
			for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
			{
				cv::Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
				cv::Point classIdPoint;
				double confidence;
				// Get the value and location of the maximum score
				minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
				if (confidence > mConfThreshold && classIdPoint.x == 0) //This network finds many pattern classes. The human class determine at number 0.
				{
					int centerX = (int)(data[0] * frame.cols);
					int centerY = (int)(data[1] * frame.rows);
					int width = (int)(data[2] * frame.cols);
					int height = (int)(data[3] * frame.rows);
					int left = centerX - width / 2;
					int top = centerY - height / 2;

					boxes.push_back(cv::Rect(left, top, width, height));
				}
			}
		}
		return boxes;
	}

	PeopleDetection::PeopleDetection(const char * classesFilePath, const char * modelConfigurationFilePath, const char * modelWeightsFilePath)
		:mClassesFile(classesFilePath),
		mModelConfiguration(modelConfigurationFilePath),
		mModelWeights(modelWeightsFilePath)
	{
		// Load names of classes		
		std::ifstream ifs(mClassesFile);
		std::string line;
		while (getline(ifs, line)) classes.push_back(line);

		// Load the network
		mNet = cv::dnn::readNetFromDarknet(mModelConfiguration, mModelWeights);
		mNet.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
		mNet.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
	}

	PeopleDetection::~PeopleDetection()
	{
	}

	std::vector<cv::Rect> PeopleDetection::Predict(const cv::Mat & frame)
	{
		cv::Mat blob;
		// Create a 4D blob from a frame.
		cv::dnn::blobFromImage(frame, blob, mScaleFactor, cvSize(416, 416), cv::Scalar(0, 0, 0), true, false);
		mNet.setInput(blob);

		// Runs the forward pass to get output of the output layers
		std::vector<cv::Mat> outs;
		mNet.forward(outs, GetOutputsNames(mNet));

		// Remove the bounding boxes with low confidence
		return postprocess(frame, outs);
	}

}
